from variables import friedman,operation,operationEtConversionDeType
from afichage import addition,poly_A,poly_A_et_GC,ecriture_formate,ecriture_formate_2
from liste import jour_de_la_semaine,table_de_multiplication,nombre_paire,saison,liste_et_indice,liste_et_range
from teste import acide_amine,complementaire_d_un_ADN,conjection_de_syracuse,jour_de_la_semaine,minimum_d_un_liste,nombre_paire,nombre_premier_inf_100,note_et_mention_d_un_etudiant,recherche_nombre_dicotomie,acide_amine_d_un_proteine
from boucle_et_comparaison import fibonacy, boucle_de_base,boucle_et_jour_de_la_semaine,calcule_de_la_moyenne,fibonacy,nombre_paire_et_impaire,nombre_sur_une_ligne,parcour_demi_matrice,produit_de_nombre_consecutif,pyramide,status_de_puce,triangle_gauche,triangle_inverse,triangle,parcours_de_matrice
from tools.console import *




def main_menu():

    while True:
        print("\n\t Choisissez le chapitre que vous voulez  ")
        choix_chapitre=input("""
    1. Variables
    2. Affichage
    3. Listes
    4. Boucles et Comparaisons
    5. Tests
    
    Entrez 'quit' pour quitter
-->""")
        if choix_chapitre=='1':
            chapitre2()
            validation()
        elif choix_chapitre=='2':
            chapitre3()
            validation()
        elif choix_chapitre=='3':
            chapitre4()
            validation()
        elif choix_chapitre=='4':
            chapitre5()
            validation()
        elif choix_chapitre=='5':
            chapitre6()
            validation()
        elif choix_chapitre=='quit':
            validation()
            print("Au revoir")

            break
        else :
            print("Choix invalide ")
            continue

def chapitre2():
    while True:
        print("Entrez le numero de l'exercice qui vous interesse")
        choix_exo_chap2=input("""
        1. Nombre de Friedman
        2. Prediction
        3. Conversions de types
        
        Entrez 'retour' pour revenir au menu principal
-->""")
        
        if choix_exo_chap2=='1':
            friedman.friedman()
            validation()
        elif choix_exo_chap2=='2':
            operation.operation()
            validation()
        elif choix_exo_chap2=='3':
            operationEtConversionDeType.resultat()
            validation()
        elif choix_exo_chap2=='retour':
            break
        else :
            print("Choix Invalide ")
            continue

def chapitre3():
    while True:
        print("Entrez le numero de l'exercice qui vous interesse")
        choix_exo_chap3=input("""    
        1. Affichage de resultat de l'addition
        2. poly-A
        3. poly-A et poly-GC
        4. Eciture formatee
        5. Ecriture formatee (2)
        
        Entrez 'retour' pour revenir au menu principal
-->""")
        if choix_exo_chap3 =='1':
            addition.addition()
            validation()
        elif choix_exo_chap3=='2':
            poly_A.adn()
            validation()
        elif choix_exo_chap3=='3':
            poly_A_et_GC.poly_A_et_poly_GC()
            validation()
        elif choix_exo_chap3=='4':
            ecriture_formate.ecriture_formate()
            validation()
        elif choix_exo_chap3=='5':
            ecriture_formate_2.ecriture_formate_2()
            validation()
        elif choix_exo_chap3=='retour':
            break
        else :
            print("Choix Invalide ")
            continue

def chapitre4():
    while True:
        print("Entrez le numero de l'exercice qui vous interesse")
        choix_exo_chap4=input("""
    1. Jour de la semaime
    2. Saisons
    3. Table de Multiplication
    4. Nombres Pairs
    5. Lists & Indices
    6. Lists & Range
    
    Entrez 'exit' pour quitter
-->""")
        if choix_exo_chap4=='1':
            jour_de_la_semaine.jour_de_la_semaine()
            validation()
        elif choix_exo_chap4=='2':
            saison.saison()
            validation()
        elif choix_exo_chap4=='3':
            table_de_multiplication.table_de_multiplication()
            validation()
        elif choix_exo_chap4=='4':
            nombre_paire.nombre_paire()
            validation()
        elif choix_exo_chap4=='5':
            liste_et_indice.liste_et_indice()
            validation()
        elif choix_exo_chap4=='6':
            liste_et_range.liste_et_range()
            validation()
        elif choix_exo_chap4=='retour':
            break
        else :
            print("Choix Invalide ")
            continue


def chapitre5():
    while True:
        print("Entrez le numero de l'exercice qui vous interesse")
        choix_exo_chap5=input("""
    1. Boucle de base
    2. Boucle et Jour de la semaine
    3. Nombre sur une ligne
    4. Nombres pairs et impairs
    5. Calcul de moyenne
    6. Produits de nombres consecutifs
    7. Triangle
    8. Triangle Inverse
    9. Triangle Gauche
    10.Pyramide
    11.Parcours de Matrice
    12.Parcours de demi-matrice
    13.Sauts de puces
    14.Suite de Fibonacci
    
    Entrez 'retour' pour revenir au menu principal
-->""")
        if choix_exo_chap5=='1':
            boucle_de_base.boucle_de_base()
            validation()
        elif choix_exo_chap5=='2':
            boucle_et_jour_de_la_semaine.boucle_et_jour_de_la_semaine()
            validation()
        elif choix_exo_chap5=='3':
            nombre_sur_une_ligne.nombre_sur_une_ligne()
            validation()
        elif choix_exo_chap5=='4':
            nombre_paire_et_impaire.nombre_paire_et_impaire()
            validation()
        elif choix_exo_chap5=='5':
            calcule_de_la_moyenne.calcule_de_la_moyenne()
            validation()
        elif choix_exo_chap5=='6':
            produit_de_nombre_consecutif.produit_de_nombre_consecutif()
            validation()
        elif choix_exo_chap5=='7':
            triangle.triangle()
            validation()
        elif choix_exo_chap5=='8':
            triangle_inverse.triangle_inverse()
            validation()
        elif choix_exo_chap5=='9':
            triangle_gauche.triangle_gauche()
            validation()
        elif choix_exo_chap5=='10':
            pyramide.pyramide()
            validation()
        elif choix_exo_chap5=='11':
            parcours_de_matrice.parcourd_de_matrice()
            validation()
        elif choix_exo_chap5=='12':
            parcour_demi_matrice.paarcour_demi_matrice()
            validation()
        elif choix_exo_chap5=='13':
            status_de_puce.status_de_puce()
            validation()
        elif choix_exo_chap5=='14':
            fibonacy.fibonacy()
            validation()
        elif choix_exo_chap5=='retour':
            break
        else :
            print("Choix Invalide ")
            continue


def chapitre6():
    while True:
        print("Entrez le numero de l'exercice qui vous interesse")
        choix_exo_chap6=input("""
    1. Jour de la semaine
    2. Complementaire d'un brin d'ADN
    3. Minimum d'une Liste
    4. Acide Amine
    5. Note et mention d'un etudiant
    6. Nombre Paire
    7. Conjecture de Syracus
    8. Acide amine d'une Proteine
    9. Nombre premiers inferieur a 100
    10.Recherche de nombre par Dichotomie
    
    
    Entrez 'retour' pour revenir au menu principal
-->""")
        if choix_exo_chap6=='1':
            jour_de_la_semaine.jour_de_la_semaine()
            validation()
        elif choix_exo_chap6=='2':
            complementaire_d_un_ADN.complementaire_d_un_ADN()
            validation()
        elif choix_exo_chap6=='3':
            minimum_d_un_liste.minuimum_d_un_liste()
            validation()
        elif choix_exo_chap6=='4':
            acide_amine.acide_amine()
            validation()
        elif choix_exo_chap6=='5':
            note_et_mention_d_un_etudiant.note_et_mention_d_un_etudiant()
            validation()
        elif choix_exo_chap6=='6':
            nombre_paire.nombre_paire()
            validation()
        elif choix_exo_chap6=='7':
            conjection_de_syracuse.conjection_de_syracuse()
            validation()
        elif choix_exo_chap6=='8':
            acide_amine_d_un_proteine.acide_amine_d_un_proteine()
            validation()
        elif choix_exo_chap6=='9':
            nombre_premier_inf_100.nombre_premier_inf_100()
            validation()
        elif choix_exo_chap6=='10':
            recherche_nombre_dicotomie.recherche_nombre_dicotomie()
            validation()
        elif choix_exo_chap6=='retour':
            break
        else :
            print("Choix Invalide ")
            continue


main_menu()