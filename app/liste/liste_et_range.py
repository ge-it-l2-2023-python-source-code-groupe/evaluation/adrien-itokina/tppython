def liste_et_range ():
    print("liste et range")
    # Initialiser lstVide comme une liste vide
    lstVide = []

    # Initialiser lstFlottant comme une liste de cinq flottants nuls
    lstFlottant = [0.0] * 5

    # Afficher les listes
    print("Liste Vide :", lstVide)
    print("Liste Flottant :", lstFlottant)

    # Ajouter à la liste lstVide les nombres entre 0 et 1000 avec un pas de 200
    lstVide += list(range(0, 1001, 200))

    # Afficher les entiers de 0 à 3
    print("Entiers de 0 à 3 :", list(range(4)))

    # Afficher les entiers de 4 à 7
    print("Entiers de 4 à 7 :", list(range(4, 8)))

    # Afficher les entiers de 2 à 8 par pas de 2
    print("Entiers de 2 à 8 par pas de 2 :", list(range(2, 9, 2)))

    # Définir lstElmnt comme une liste des entiers de 0 à 5
    lstElmnt = list(range(6))

    # Ajouter le contenu des deux listes à la fin de la liste lstElmnt
    lstElmnt.extend(lstVide + list(range(6)))

    # Afficher le contenu final de lstElmnt
    print("Liste des elements après ajout du contenu des deux listes :", lstElmnt)
