def note_et_mention_d_un_etudiant():
    print("note et mention d un etudiant")
    # Notes de l'étudiant
    notes = [14, 9, 13, 15, 12]

    # Calcul de la note maximale, minimale et de la moyenne
    note_max = max(notes)
    note_min = min(notes)
    moyenne = sum(notes) / len(notes)

    # Affichage des résultats
    print(f"Note maximale : {note_max}")
    print(f"Note minimale : {note_min}")
    print(f"Moyenne : {moyenne:.2f}")

    # Attribution de la mention
    if 10 <= moyenne < 12:
        mention = "passable"
    elif 12 <= moyenne < 14:
        mention = "assez bien"
    elif moyenne >= 14:
        mention = "bien"
    else:
        mention = "non décernée"

    print(f"Mention obtenue : {mention}")
