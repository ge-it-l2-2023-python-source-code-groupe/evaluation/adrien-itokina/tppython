def nombre_premier_inf_100():
    print("nombre premier inferieur a 100")
    print("methode 1")
    nombres_premiers = []

    for nombre in range(2, 101):
        est_premier = True
        for i in range(2, int(nombre**0.5) + 1):
            
            if nombre % i == 0:
                est_premier = False
                break
        
        if est_premier:
            print(nombre, end= ' ')

    
    print()


    print("methode 2")

    nombres_premiers = [2]

    for nombre in range(3, 101):
        est_compose = False

        for premier in nombres_premiers:
            if nombre % premier == 0:
                est_compose = True
                break

        if est_compose:
            print(f"{nombre} est compose.")
    
        else:
             nombres_premiers.append(nombre)

    print("Liste des nombres premiers jusqu'a 100;", nombres_premiers)
