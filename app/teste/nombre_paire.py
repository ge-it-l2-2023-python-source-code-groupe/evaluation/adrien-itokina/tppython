def nombre_paire():
    print("nombre paire")
    # Parcours des nombres de 0 à 20
    for nombre in range(21):
        # Vérifier si le nombre est pair
        if nombre % 2 == 0:
            # Afficher les nombres pairs inférieurs ou égaux à 10
            if nombre <= 10:
                print(f"Nombre pair inférieur ou égal à 10 : {nombre}")
        else:
            # Afficher les nombres impairs strictement supérieurs à 10
            if nombre > 10:
                print(f"Nombre impair strictement supérieur à 10 : {nombre}")
