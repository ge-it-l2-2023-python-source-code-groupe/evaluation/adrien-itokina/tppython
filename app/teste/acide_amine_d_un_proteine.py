def acide_amine_d_un_proteine():
    print("acide amine d un proteine")
    # Liste des valeurs des angles phi et psi pour les acides aminés
    angles_phi_psi = [
        [48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
        [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
        [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
        [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
        [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]
    ]

    # Fonction pour déterminer si un acide aminé est en hélice
    def est_en_helice(phi, psi):
        tolérance = 30.0
        helice_alpha_phi = -57.0
        helice_alpha_psi = -47.0

        phi_dans_tolérance = helice_alpha_phi - tolérance <= phi <= helice_alpha_phi + tolérance
        psi_dans_tolérance = helice_alpha_psi - tolérance <= psi <= helice_alpha_psi + tolérance

        return phi_dans_tolérance and psi_dans_tolérance

    # Boucle pour traiter chaque acide aminé
    for i, angles in enumerate(angles_phi_psi, start=1):
        phi, psi = angles
        if est_en_helice(phi, psi):
            print(f"Acide aminé {i}: {angles} est en hélice.")
        else:
            print(f"Acide aminé {i}: {angles} n'est pas en hélice.")
