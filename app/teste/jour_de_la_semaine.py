def jour_de_la_semaine():
    print("jour de la semaine")
    semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

    for jour in semaine:
        if jour in ["Lundi", "Mardi", "Mercredi", "Jeudi"]:
            print(f"Au travail, c'est {jour}.")
        elif jour == "Vendredi":
            print("Chouette, c'est vendredi !")
        else:
            print("Repos ce week-end.")

