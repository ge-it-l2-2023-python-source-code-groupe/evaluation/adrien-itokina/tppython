def complementaire_d_un_ADN():
    print("complementaire d un adn")
    # Séquence d'ADN
    sequence_adn = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]

    # Dictionnaire des paires complémentaires
    complementaires = {"A": "T", "T": "A", "C": "G", "G": "C"}

    # Transformation de la séquence en séquence complémentaire
    sequence_complementaire = [complementaires[base] for base in sequence_adn]

    # Affichage des résultats
    print("Séquence d'ADN d'origine :", sequence_adn)
    print("Séquence complémentaire :", sequence_complementaire)


