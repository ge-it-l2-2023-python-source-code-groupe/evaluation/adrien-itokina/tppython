def parcourd_de_matrice():
    print("PARCOUR DE MATRICE")
    print("boucle for")
    # Définir la dimension de la matrice
    dimension = 10

    # Affichage des en-têtes
    print(f"{'ligne':>4} {'colonne':>4}")

    # Parcourir chaque élément de la matrice avec des boucles for
    for i in range(1, dimension + 1):
        for j in range(1, dimension + 1):
            print(f"{i}{j}")

    # Saut de ligne pour améliorer la lisibilité
    print()


    print("boucle while")
    # Définir la dimension de la matrice
    dimension = 10

    # Affichage des en-têtes
    print(f"{'ligne':>4} {'colonne':>4}")

    # Parcourir chaque élément de la matrice avec des boucles while
    i = 1
    while i <= dimension:
        j = 1
        while j <= dimension:
            print(f"{i}{j}")
            j += 1
        i += 1

    # Saut de ligne pour améliorer la lisibilité
    print()
