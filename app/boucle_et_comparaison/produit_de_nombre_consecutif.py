def produit_de_nombre_consecutif():
    print("Produit de nombres consécutifs")
    # Création de la liste des nombres entiers pairs de 2 à 20 inclus
    entiers = list(range(2, 21, 2))

    # Affichage de la liste entiers
    print("Liste des nombres entiers pairs de 2 à 20 inclus :", entiers)

    # Calcul du produit des nombres consécutifs deux à deux
    produits = []
    for i in range(len(entiers) - 1):
        produit = entiers[i] * entiers[i + 1]
        produits.append(produit)

    # Affichage des produits
    print("\nProduit des nombres consécutifs deux à deux :")
    for produit in produits:
        print(produit)
