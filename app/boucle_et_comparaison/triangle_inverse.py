def triangle_inverse():
    print("triangle inverser ")
    # Nombre de lignes du triangle inversé
    nombre_lignes = 10

    # Dessiner le triangle inversé
    for i in range(nombre_lignes, 0, -1):
        print('*' * i)
