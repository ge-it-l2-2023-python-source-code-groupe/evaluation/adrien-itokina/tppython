def boucle_de_base ():
    print("boucle de base")
    animaux = ["vache", "souris", "levure", "bacterie"]

    # Méthode 1 avec for et in
    print("Méthode 1 avec for et in:")
    for animal in animaux:
        print(animal)


    # Méthode 2 avec for et range
    print("\nMéthode 2 avec for et range:")
    for i in range(len(animaux)):
        print(animaux[i])


    # Méthode avec while
    print("\nMéthode avec while:")
    index = 0
    while index < len(animaux):
        print(animaux[index])
        index += 1
