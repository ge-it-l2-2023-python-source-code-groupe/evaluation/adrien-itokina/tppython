def triangle_gauche():
    print("triangle gauche")
    # Nombre de lignes du triangle
    nombre_lignes = 10

    # Dessiner le triangle incliné vers la gauche
    for i in range(1, nombre_lignes + 1):
        espaces = ' ' * (nombre_lignes - i)
        etoiles = '*' * i
        ligne = espaces + etoiles
        print(ligne)
