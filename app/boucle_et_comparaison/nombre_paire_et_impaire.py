def nombre_paire_et_impaire():
    print("Nombres pairs et impairs")
    # Liste impairs
    impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]

    # Construire la liste pairs en incrémentant chaque élément de impairs de 1
    pairs = [nombre + 1 for nombre in impairs]

    # Afficher les deux listes
    print("Liste impairs :", impairs)
    print("Liste pairs (incrémentés de 1) :", pairs)
