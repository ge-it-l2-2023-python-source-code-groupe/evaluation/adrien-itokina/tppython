def nombre_sur_une_ligne ():
    print("Nombres de 1 à 10 sur une ligne")
    # Affichage des nombres de 1 à 10 sur une seule ligne avec une boucle
    for nombre in range(1, 11):
        print(nombre, end=" ")

    # Saut de ligne à la fin pour une meilleure lisibilité
    print()
