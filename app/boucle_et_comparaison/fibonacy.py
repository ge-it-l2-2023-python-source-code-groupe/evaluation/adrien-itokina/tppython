def fibonacy():
    def fibonacci(n):
        print ("suite de fibonaci")
        fibo = [0, 1]
        for i in range(2, n):
            fibo.append(fibo[i - 1] + fibo[i - 2])
        return fibo

    # Construire la liste des 15 premiers termes de la suite de Fibonacci
    fibo = fibonacci(15)

    # Afficher la liste des 15 premiers termes de la suite de Fibonacci
    print("Liste des 15 premiers termes de la suite de Fibonacci:")
    print(fibo)

    # Afficher le rapport pour chaque élément de rang n avec n > 1
    print("\nRapports pour chaque élément de rang n avec n > 1:")
    for i in range(2, len(fibo)):
        rapport = fibo[i] / fibo[i - 1]
        print(f"Rapport pour le terme de rang {i}: {rapport}")
